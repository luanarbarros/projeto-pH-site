
<?php

include("header.php");

session_start();

$usuario = null;

if(isset($_SESSION['usuario'])) {
	$usuario = $_SESSION['usuario'];
} else {
	$_SESSION['msg'] = "You do not have permission to view this page!";
	header("location: index.php");
}

require_once('database.php');

// abre conexão com o banco de dados
$connection = connect();

$results = [];
$userId = $usuario['id'];

foreach (['pool', 'plantation', 'water-tank'] as $local) {
	$sql = "SELECT id, time, valueph FROM results where location = '${local}' and users_id = ${userId};";
	$results[$local] = $connection->query($sql)->fetchAll(PDO::FETCH_ASSOC);
}

?>

</section>

<!-- Main -->
<section id="main">
	<div class="container">

		<!-- Content -->
			<article class="box post">
				<header>
					<h2>Send Image</h2>
				</header>
				<form action="process.php" method="post" enctype="multipart/form-data">
					<input type="file" name="imagePH"><br>
					<select name="location">
						<option value="pool">Pool</option>
						<option value="plantation">Plantation</option>
						<option value="water-tank">Water-Tank</option>
					</select>
					<input type="submit" value="Submit">
				</form>
			</article>

			<article class="box post">
				<header>
					<h2>History</h2>
				</header>
				<h3>Pool</h3>
				<p>
				<?php
					echo "<pre>".json_encode($results['pool'])."</pre>";
				?>
				</p>
				<h3>Plantation</h3>
				<p>
				<?php
					echo "<pre>".json_encode($results['plantation'])."</pre>";
				?>
				</p>
				<h3>Water-Tank</h3>
				<p>
				<?php
					echo "<pre>".json_encode($results['water-tank'])."</pre>";
				?>
				</p>
			</article>

	</div>
</section>


<?php include("footer.php"); ?>