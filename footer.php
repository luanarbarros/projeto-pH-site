			<!-- Footer -->
            <section id="footer">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<!-- Copyright -->
								<div id="copyright">
										<ul class="links">
											<li>&copy; pHMeterOnline. All rights reserved.</li><li>Design: <a href="http://html5up.net">AlphaTeam</a></li>
										</ul>
									</div>
							</div>
						</div>
					</div>
				</section>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>
