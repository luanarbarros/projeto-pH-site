# Projeto pH Online

Esse projeto se trata da implementação de uma página web desenvolvida especialmente para pessoas interessadas em manter o registro do pH de ambientes como piscinas, plantações e
caixas de água. Essa aplicação também é útil para pessoas com restrição ocular, como o daltonismo, por exemplo. O funcionamento principal se da por meio do processamento
digital de imagem, no qual uma imagem de uma fita indicadora de pH juntamente com sua cartela de referência é selecionada da galeria do computador e enviada para o servidor,
onde será processada utilizando um algoritmo desenvolvido no octave, via comando shell exec no php. O resultado retornado é o valor do pH da amostra.

O site permite que o usuário tenha um login, com a finalidade de registrar o local, a data e a hora na qual a amostra foi enviada para a página, mantendo um histórico dos
valores medidos anteriormente em cada ambiente.


## Instalar inicialmente:
* Instalar Vagrant
* Instalar Octave 4.0 (instrucoes: https://askubuntu.com/a/645718)
* Instalar pacotes do Octave `io`, `statistics` e `image` (abrir o Octave como `root`: `sudo octave`, e depois executar dentro do Octave `pkg install -global -forge io`)
* Alterar as permissoes da pasta www para o usuario `www-data`: `sudo chown -R www-data:www-data /var/www`

## Criar banco de dados:

create database monitor_db;

use monitor_db;

create table users ( id int NOT NULL AUTO_INCREMENT, name varchar(100) NOT NULL, email varchar(100) NOT NULL, password varchar(100) NOT NULL, PRIMARY KEY (id) );

create table results ( id int NOT NULL AUTO_INCREMENT, time datetime NOT NULL, valueph int NOT NULL, location varchar(255) NOT NULL, users_id int NOT NULL, PRIMARY KEY (id), KEY user s_id(users_id), CONSTRAINT fk_users_id FOREIGN KEY (users_id) REFERENCES users (id));

## Screenshot

![Tela de login](screenshot/screenshot.png)