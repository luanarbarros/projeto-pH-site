<?php include("header.php");?>

			<!-- Main -->
			<section id="main">
					<div class="container">

						<!-- Content -->
						<article class="box post">
								<!-- <a href="#" class="image featured"><img src="images/pic01.jpg" alt="" /></a> -->
								<header>
									<h2>Welcome</h2>
									<!-- <p>Lorem ipsum dolor sit amet feugiat</p> -->
								</header>
								<p>
									Welcome to pH Meter Online. This site was developed especially for people who are interested in keeping track of
									the pH of your pool, plantation, or water tank. This site is also useful for people with some type of eye restriction,
									such as color blindness.
								</p>
								<p>
									The use of this site for pH reading is restricted to ribbons of the Macherey-Nagel Ref 921 10 pH-Fix 0 - 14 brand,
									as shown in the image below.

								</p>
								<!-- <span>
									<a href="#" class="image featured"><img src="images/pic01.jpg" alt="" /></a>
								</span> -->

								<section>
									<header>
										<h3>Something else</h3>
										<a href="#" class="image featured"><img src="images/pic01-ph.jpg" alt="" /></a>
									</header>
									<!-- <p>
										Elementum odio duis semper risus et lectus commodo fringilla. Maecenas sagittis convallis justo vel mattis.
										placerat, nunc diam iaculis massa, et aliquet nibh leo non nisl vitae porta lobortis, enim neque fringilla nunc,
										eget faucibus lacus sem quis nunc suspendisse nec lectus sit amet augue rutrum vulputate ut ut mi. Aenean
										elementum, mi sit amet porttitor lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor
										sit amet nullam consequat feugiat dolore tempus.
									</p>
									<p>
										Nunc diam iaculis massa, et aliquet nibh leo non nisl vitae porta lobortis, enim neque fringilla nunc,
										eget faucibus lacus sem quis nunc suspendisse nec lectus sit amet augue rutrum vulputate ut ut mi. Aenean
										elementum, mi sit amet porttitor lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor
										sit amet nullam consequat feugiat dolore tempus.
									</p> -->
								</section>
								<section>
									<header>
										<h3>How to Use Our Services</h3>
									</header>
									<p>
									First sign up for this site on the main page. Then log in. Go to the "Get Started" button and select an
									 image from your gallery.
									</p>
								</section>

								<section>
									<header>
										<h3>How to Photograph Your pH Strip</h3>
									</header>
									<p>
									After immersing your indicator tape on the substance to be tested for pH, place it on a white paper along with
									 the reference table on the packaging itself as specified in the following image.
									</p>
								</section>
								<section>
									<header>
									<h3>How to Use Our Services</h3>
									<a href="#" class="image featured"><img src="images/pic01-ph2.jpg" alt="" /></a>
									</header>
									<p>
									Ready! Take the picture and wait for the processing of your result for a few seconds.
									</p>
								</section>
							</article>

					</div>
				</section>


<?php include("footer.php");?>