<?php

    session_start();

    if(!isset($_POST["username"],$_POST["email"],$_POST["pass"])){
        $_SESSION['msg'] = "Incorrect information";
        header("location: index.php");
        return;
    }

    $user = $_POST["username"];
    $email = $_POST["email"];
    $pass = $_POST["pass"];

    $user = trim($user);
    $email = trim($email);
    $pass = trim($pass);

    if(empty($user) || empty($email) || empty($pass)){
        $_SESSION['msg'] = "Fill in all the fields";
        header("location: index.php");
        return;
    }

    $pass = crypt($pass, "123456");

    require_once('database.php');

    // abre conexão com o banco de dados
    $connection = connect();

    // pesquisar se existe usuário cadastrado com email fornecido
    $sql = "SELECT * FROM users where email = '${email}';";
    
    // executa a consulta
    $result = $connection->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    // verifica o resultado da consulta
    $jaExiste = count( $result ) > 0;

    if($jaExiste) {
        $_SESSION['msg'] = "User already registered";
        header("location: index.php");
        return;
    }

    $sql = "INSERT INTO users (name, email, password) VALUES ('${user}', '${email}', '${pass}');";
  
    $id = 0;

    try {
        $connection->exec($sql);
        $id = $connection->lastInsertId();
    } catch(PDOExecption $e) { 
        $connection->rollback(); 
        $_SESSION['msg'] = "Error!: " . $e->getMessage(); 
        header("location: index.php");
    }

      $_SESSION['msg'] = "Registered user";
      header("location: index.php");
?>