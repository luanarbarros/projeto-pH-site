<?php
	
	session_start();

    if(!isset($_POST["email"],$_POST["pass"])) {
		$_SESSION['msg'] = "Incorrect information";
        header("location: index.php");
        return;
    }

    $email = $_POST["email"];
    $pass = $_POST["pass"];

    $email = trim($email);
    $pass = trim($pass);

    if(empty($email) || empty($pass)){
		$_SESSION['msg'] = "Fill in all the fields";
        header("location: index.php");
        return;
    }

    require_once('database.php');

    // abre conexão com o banco de dados
    $connection = connect();

    // pesquisar se existe usuário cadastrado com email fornecido
    $sql = "SELECT * FROM users where email = '${email}';";
    
    // executa a consulta
    $result = $connection->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    // verifica o resultado da consulta
    $estaCadastrado = count( $result ) > 0;

    if(!$estaCadastrado) {
		$_SESSION['msg'] = "User not yet registered";
        header("location: index.php");
        return;
	}
	
	$usuario = $result[0];

	if (crypt($pass, $usuario['password']) != $usuario['password']) {
		$_SESSION['msg'] = "Invalid password";
		header("location: index.php");
        return;
	}

	$_SESSION['usuario'] = $usuario;
	$_SESSION['msg'] = "Welcome!";
	
	header("location: index.php");
	

?>