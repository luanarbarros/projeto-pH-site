<?php

session_start();

date_default_timezone_set("America/Recife");

$usuario = null;

if(isset($_SESSION['usuario'])) {
	$usuario = $_SESSION['usuario'];
} else {
	$_SESSION['msg'] = "Você não tem permissão para ver esta página!";
	header("location: index.php");
}


$target_dir = "uploads/";
$uploadOk = 1;
// Define a extensão do arquivo
$imageFileType = strtolower(pathinfo($_FILES["imagePH"]["name"],PATHINFO_EXTENSION));

// echo "<pre>";
// echo var_dump($_FILES);
// echo "</pre>";
// die();

//Criando o nome do arquivo
// $target_file = $target_dir . date("YmdHis") . "." . $imageFileType;
$target_file = "process.jpg";


$mensagem = "";


// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["imagePH"]["tmp_name"]);
    if($check !== false) {
        $mensagem .= "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        $mensagem .= "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    unlink($target_file);
}
// Check file size
if ($_FILES["imagePH"]["size"] > 500000) {
    $mensagem .= "Sorry, your file is too large.";
    $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "jpg") {
    $mensagem .= "Sorry, only JPG files are allowed.";
    $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    $mensagem .= "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["imagePH"]["tmp_name"], $target_file)) {
        $mensagem .= "The file ". basename( $_FILES["imagePH"]["name"]). " has been uploaded. ";
        $ph = shell_exec("octave --path ph-octave --silent ph-octave/run.m 2>&1");
        $ph = trim($ph);

        if(!is_numeric($ph)) {
            $mensagem .= "Sorry, the file could not be processed.";
            unset($ph);
        } else {
            $ph = intval($ph);

            $location = $_POST['location'];
            $userId = $usuario['id'];

            // inserir no banco de dados
            require_once('database.php');

            // abre conexão com o banco de dados
            $connection = connect();

            // pesquisar se existe usuário cadastrado com email fornecido
            $sql = "INSERT INTO results (time, valueph, location, users_id)
                        VALUES ('".date("Y-m-d H:i:s")."', '${ph}', '${location}', '${userId}');";
            
            // insere no banco de dados
            try {
                $connection->exec($sql);
            } catch(PDOExecption $e) {
                $connection->rollback(); 
                $mensagem .= " Error!: " . $e->getMessage();
            } 
        }

    } else {
        $mensagem .= "Sorry, there was an error uploading your file.";
    }
}
?>


<?php include("header.php");?>

</section>

<!-- Main -->
<section id="main">
<div class="container">

<!-- Content -->
<article class="box post">
<header>
    <h2>Resultado</h2>
</header>
<p><?php echo $mensagem; ?></p>
<p><?php if(isset($ph)) echo "Valor do pH: $ph"; ?></p>
<p> <img src="<?php echo $target_file; ?>"> </p>
<p><a href="submit.php" class="button large">Enviar outra imagem</a></p>
</article>

</div>
</section>


<?php include("footer.php"); ?>