<?php include("header.php"); ?>

			<!-- Main -->
				<section id="main">
					<div class="container">

						<!-- Content -->
						<article class="box post">

								<section>
									<header>
										<h3>Alpha Team Developers</h3>
									</header>
									<p>
										Luana Rodrigues
									</p>

									<p>
										Phellype Amorim
									</p>
									<p>
										Kadu Silva
									</p>
								</section>
								<section>
									<header>
										<h3>Digital Image Processing</h3>
									</header>
									<p>
										Luana Rodrigues
									</p>

									<p>
										Adriano Soares
									</p>
									<p>
										Gabriel Gutierrez
									</p>
								</section>
								<section>
									<header>
										<h3>Acknowledgment</h3>
									</header>
									<p>
										Gabriel Gutierrez
									</p>
								</section>
							</article>

					</div>
				</section>

			<!-- Footer -->
			<?php include("footer.php"); ?>