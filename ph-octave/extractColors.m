function [sample,L_ref,a_ref,b_ref] = extractColors(inputImg, sqSample, sqRef)
    
    % Transforma a imagem para o espaco de cor CIELab
    imgLab = rgb2lab(inputImg);

    % Para cada quadrado da amostra
    for sq = 1:4
        % pega a porcao da imagem correspondente
        sqImg = imgLab(sqSample(sq,1):sqSample(sq,3),sqSample(sq,2):sqSample(sq,4),:);
        
        % Calcula a primeira media, contendo os outliers
        meanLab = zeros([1 3]);
        meanLab(:,1) = mean2(sqImg(:,:,1));
        meanLab(:,2) = mean2(sqImg(:,:,2));
        meanLab(:,3) = mean2(sqImg(:,:,3));
        
        [X, Y, ~] = size(sqImg);
        
        % Percorre todos os pixels        
        for j = 1:Y
            for i = 1:X
                % Cor do pixel (i,j) 
                temp = sqImg(i, j,:);
                temp = temp (:);
                temp = double(temp');

                % Caso a distancia euclidiana do pixel com a media seja maior
                % que 20, o pixel eh onsiderado como outlier e zerado
                if(pdist2 (meanLab, temp) >= 20)
                    sqImg(i, j,:) = 0;
                endif
            endfor
        endfor
        
        Lab_L = sqImg(:, :,1);
        Lab_a = sqImg(:, :,2);
        Lab_b = sqImg(:, :,3);
        
        % Calcula a media sem os outliers (zerados anteriormente)
        L_sample(sq) = mean(nonzeros(Lab_L(:)));
        a_sample(sq) = mean(nonzeros(Lab_a(:)));
        b_sample(sq) = mean(nonzeros(Lab_b(:)));

        % Detecta qual o quadrado da ref possui a cor mais proxima
        for refCol = 1:16
            % Pega o quadrado da sqRef
            sqImg = imgLab(sqRef(sq,1, refCol):sqRef(sq,3, refCol),sqRef(sq,2,refCol):sqRef(sq,4,refCol),:);

            % Calcula a primeira media, contendo os outliers
            meanLab = zeros ([1 3]);
            meanLab(:,1) = mean2 (sqImg(:,:,1));
            meanLab(:,2) = mean2 (sqImg(:,:,2));
            meanLab(:,3) = mean2 (sqImg(:,:,3));
            
            [X, Y, ~] = size (sqImg);
            
            %Percorre todos os pixels
            for i = 1:X
                for j = 1:Y
                    %variavel para receber os valores de cada banda do pixel nas coordenadas (i,j)
                    temp = sqImg(i, j,:);
                    temp = temp (:);
                    temp = double(temp');

                    %Caso a distancia euclidiana do pixel com a media seja maior
                    %que 20, o pixel eh onsiderado como outlier e zerado
                    if (pdist2 (meanLab, temp) >= 20)
                        sqImg(i, j,:) = 0;
                    endif
                endfor
            endfor
            
            Lab_L = sqImg(:, :,1);
            Lab_a = sqImg(:, :,2);
            Lab_b = sqImg(:, :,3);
            
            %Recalcula a media, agora sem os outliers, e armazena em um array para cada banda de cor
            L_ref(refCol,sq) = mean (nonzeros(Lab_L(:)));
            a_ref(refCol,sq) = mean (nonzeros(Lab_a(:)));
            b_ref(refCol,sq) = mean (nonzeros(Lab_b(:)));
        endfor
        
        %Transforma os 3 arrays de 4 elementos contendo as medias da fita para
        %uma matrix 4x3, onde cada linha contem as medias de um quadrado
    endfor
    
    sample = [L_sample; a_sample; b_sample]';

endfunction