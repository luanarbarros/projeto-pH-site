function [squaresSample, squaresRef] = detectSquares(img)

    %
    % ==========================================================================
    % PARTE 1: tratamento, identificacao de bordas e conversao para BW

    [nRows, nCols, ~] = size(img);

    % Deteccao de bordas e normalizacao
    imgEdges = coloredges(img);
    imgEdges = imgEdges / max(imgEdges(:));

    % Conversao para BW, com threshold = 30
    imgBW = 255*imgEdges > 30;
    
    openImg(1 - imgBW);
    
    % ==========================================================================
    % PARTE 2: deteccao das divisoes horizontais

    % Histograma horizontal
    hHistRaw = zeros([nRows 1]);    % histograma raw
    hHist = zeros([nRows 1]);       % 1a derivada do raw, que sera utilizado efetivamente no processo

    % Gera imagem para facilitar visualizacao
    hHistRawImg = zeros([nRows nCols 3]);
    hHistImg = zeros([nRows nCols 3]);

    % Registra o maior espaco entre as ocorrencias do histograma, que define a
    % separacao entre a tira da amostra e a cartela de referencia
    lastPeak = 0;           % ultimo valor nao-nulo encontrado
    biggerSpace = 0;        % tamanho do maior espaco
    biggerSpaceStart = 0;   % onde comeca o maior espaco

    % calcula o histograma e a derivada, ignorando a primeira e ultima linhas
    for i = 2:(nRows-1)
        % qtd de pixels brancos em cada linha = [0, 255]
        hHistRaw(i) = uint8(255 * mean(imgBW(i,:)));
        h = hHistRaw(i);

        % gera imagem do histograma, preenchendo cada linha com pixels brancos
        % de acordo com o valor do histograma
        hHistRawImg(i, 1:(uint16(h*nCols/255.0)), :) = 1;

        % calcula 1a derivada, ignorando diferenca menor que 10
        dh = h - hHistRaw(i-1);
        if (dh > 10)
            hHist(i) = dh;
        else
            dh = 0;
        endif
        
        % gera imagem da derivada do histograma
        %hHistImg(i, 1:(uint16(dh*nCols/255.0)), :) = 1;
        
        % registra o maior espaco entre valores nao-nulos da derivada
        if (dh > 0)
            if (lastPeak > 0 && (i - lastPeak) > biggerSpace)
                biggerSpace = i - lastPeak;
                biggerSpaceStart = lastPeak;
            endif
            lastPeak = i;
        endif
    end

    % linha de corte fica na metade do maior espaco
    cutRow = uint16(biggerSpaceStart + biggerSpace/2);

    % mostra a separacao nas imagens do histograma, com uma linha azul
    %hHistImg((cutRow-1):(cutRow+1), :, 1) = 0.2;
    %hHistImg((cutRow-1):(cutRow+1), :, 2) = 0.2;
    %hHistImg((cutRow-1):(cutRow+1), :, 3) = 1;

    % 0 indica que a tira esta acima, 1 indica que esta abaixo da referencia
    isRefAbove = sum(hHist(1:cutRow)) > sum(hHist(cutRow:nRows));

    % -----------------------------
    % Identificacao das divisoes horizontais
    hLinesAll = [];     % indices de todas as divisoes horizontais
    lineStart = 0;      % indice onde comeca uma borda no histograma
    zeroCount = 0;      % qtd de zeros consecutivos

    for i = 1:nRows
        % marca inicio de uma borda no histograma
        if (hHist(i) ~= 0 && lineStart == 0)
            lineStart = i;
        
        % se ja foi detectado o inicio da borda, e se ja passaram 5 zeros consecutivos,
        % a borda chegou ao fim; escolhe entao a linha com o maior valor do histograma
        % nesse intervalo
        elseif (lineStart ~= 0 && zeroCount > 4)
            maxValue = hHist(lineStart);
            maxRow = lineStart;
            for j = lineStart:i
                if (hHist(j) > maxValue)
                    maxValue = hHist(j);
                    maxRow = j;
                endif
            endfor
            
            % adiciona o indice no array de divisoes
            hLinesAll = [hLinesAll; maxRow];
            
            % pinta a linha na imagem do histograma
            hHistImg((maxRow-1):(maxRow+1), :, 1) = 1;
            
            % reinicia o processo
            lineStart = 0;
        endif
        
        % conta os zeros consecutivos
        if (hHist(i) == 0)
            zeroCount++;
        else
            zeroCount = 0;
        endif
    endfor

    % -----------------------------
    % identificacao das linhas que delimitam as cores (ignorando as linhas
    % que delimitam a cartela de referencia), atraves do espaco entre elas:
    % sao as linhas de interesse

    spaces = [];    % cada linha guarda o espaco, o indice inicial e o final
    hLines = [];    % indices das linhas horizontais de interesse

    % percorre cada linha encontrada
    for i = 2:size(hLinesAll, 1)
        h1 = hLinesAll(i-1);
        h2 = hLinesAll(i);
        
        % ignora o espaco maior, onde esta a linha de corte
        if (h1 < cutRow && h2 > cutRow)
            continue;
        endif
        
        % registra o espaco
        spaces = [spaces; (h2 - h1) h1 h2];
    endfor

    % calcula o espaco medio, e entao as linhas de interesse sao aquelas
    % cujo espaco esta acima da media
    avgSpace = mean(spaces(:, 1));

    % pinta as linhas de interesse de amarelo e magenta na imagem do histograma
    
    for i = 1:size(spaces, 1)
        if (spaces(i, 1) > avgSpace)
            hLines = [hLines; spaces(i, 2) spaces(i, 3) ];
            %hHistImg((spaces(i, 2)-1):(spaces(i, 2)+1), :, 2) = 1;
            %hHistImg((spaces(i, 3)-1):(spaces(i, 3)+1), :, 3) = 1;
        endif
    endfor
    
    
    % ==========================================================================
    % PARTE 3: deteccao das divisoes verticais

    % Agora os histogramas sao divididos em duas partes, uma para a referencia
    % e outra para a tira da amostra
    vHistRaw = zeros([2 nCols]);
    vHist = zeros([2 nCols]);

    % Criacao de imagens
    vHistRawImg = zeros([nRows nCols 3]);
    vHistImg = zeros([nRows nCols 3]);

    % Desenho das linhas horizontais para melhor visualizacao
    for i = 1:size(hLines, 1)
        line1 = hLines(i, 1);
        line2 = hLines(i, 2);
        
        #vHistRawImg(line1-1:line1, :, 1) = 1;
        #vHistImg(line1-1:line1, :, 1) = 1;
        
        #vHistRawImg(line2-1:line2, :, 1) = 1;
        #vHistImg(line2-1:line2, :, 1) = 1;
    endfor

    % Identificacao das divisoes verticais
    vLinesRefAll = [];          % indices de todas as colunas da cartela de referencia
    vLinesSampleAll = [];       % indices de todas as colunas da amostra

    % Para cada parte (referencia e amostra)...
    for i = 1:2
        
        % Define as faixas onde serao feitos os histogramas
        if i == 1
            line1 = hLines(1, 1);      % primeira linha encontrada
            if isRefAbove == 1          % amostra abaixo da referencia
                line2 = hLines(4, 2);      % ultima linha da referencia
            else
                line2 = hLines(1, 2);      % ultima linha da amostra
            endif
        else
            if isRefAbove == 1          % amostra abaixo da referencia
                line1 = hLines(5, 1);      % primeira linha da amostra
            else
                line1 = hLines(2, 1);      % primeira linha da referencia
            endif
            line2 = hLines(5, 2);      % ultima linha encontrada
        endif
        
        % ignora primeira e ultima colunas
        for j = 2:(nCols-1)
            % calcula o histograma
            vHistRaw(i, j) = uint8( 255 * mean( imgBW(line1:line2, j) ) );
            h = vHistRaw(i, j);
            
            % desenha uma linha somente se nao for nulo
            if (h > 0)
                vHistRawImg((line2 - uint16(h*(line2 - line1)/255.0)):line2, j, :) = 1;
            endif
            
            % calculo da 1a derivada, ignorando valores menores que 20
            dh = h - vHistRaw(i, j-1);
            if (dh > 20)
                vHist(i, j) = dh;
                
                % desenha a linha na imagem do histograma
                %vHistImg((line2 - uint16(dh*(line2 - line1)/255.0)):line2, j, :) = 1;
            endif
        endfor
        
        colStart = 0;       % indice onde comeca uma borda no histograma
        zeroCount = 0;      % qtd de zeros consecutivos
        
        for j = 1:nCols
            % marca inicio de uma borda no histograma
            if (vHist(i, j) > 0 && colStart == 0)
                colStart = j;
            
            % se ja foi detectado o inicio da borda, e se ja passaram 5 zeros consecutivos,
            % a borda chegou ao fim; escolhe entao a linha com o maior valor do histograma
            % nesse intervalo
            elseif (colStart > 0 && zeroCount > 5)
                maxValue = vHist(i, colStart);
                maxCol = colStart;
                for k = colStart:j
                    if (vHist(i, k) > maxValue)
                        maxValue = vHist(i, k);
                        maxCol = k;
                    endif
                endfor
                
                % adiciona o indice no array de divisoes
                if (i == 1 && isRefAbove) || (i == 2 && ~isRefAbove)
                    vLinesRefAll = [vLinesRefAll; maxCol];
                else
                    vLinesSampleAll = [vLinesSampleAll; maxCol];
                endif
                
                % pinta a linha na imagem do histograma
                %vHistImg(line1:line2, maxCol-1:maxCol, 2) = 1;
                
                % reinicia o processo
                colStart = 0;
            endif
            
            % conta os zeros consecutivos
            if (vHist(i, j) == 0)
                zeroCount++;
            else 
                zeroCount = 0;
            endif
        endfor
    endfor

    % -----------------------------
    % identificacao das colunas que delimitam as faixas de referencia e as cores
    % da amostra, atraves do espaco entre elas: sao as colunas de interesse

    vLinesRef = [];         % cada linha guarda um par de colunas de interesse,
    vLinesSample = [];      %   delimitando as areas coloridas

    % Para cada parte (referencia e amostra)...
    for i = 1:2
        % o algoritmo e o mesmo para a referencia e a amostra, entao trata como vLines
        if (i == 1 && isRefAbove) || (i == 2 && ~isRefAbove)
            vLines = vLinesRefAll;
        else
            vLines = vLinesSampleAll;
        endif
        
        spaces = [];    % cada linha guarda o espaco, o indice inicial e o final

        % percorre cada coluna encontrada
        for j = 2:size(vLines, 1)
            spaces = [spaces; (vLines(j) - vLines(j-1)) vLines(j-1) vLines(j)];
        endfor

        % calcula o espaco medio, e entao as colunas de interesse sao aquelas
        % cujo espaco esta acima da media
        avgSpace = mean(spaces(:, 1));
        vLines2 = [];       % colunas de interesse

        % pinta as colunas de interesse de azul (inicio) e vermelho (fim)
        % na imagem do histograma
        
        for j = 1:size(spaces, 1)
            if (spaces(j, 1) > avgSpace)
                vLines2 = [vLines2; spaces(j, 2) spaces(j, 3) ];
                
                if i == 1
                    vHistImg(1:cutRow, spaces(j, 2)-1:spaces(j, 2), 3) = 1;
                    vHistImg(1:cutRow, spaces(j, 3)-1:spaces(j, 3), 1) = 1;
                else
                    vHistImg(cutRow:nRows, spaces(j, 2)-1:spaces(j, 2), 3) = 1;
                    vHistImg(cutRow:nRows, spaces(j, 3)-1:spaces(j, 3), 1) = 1;
                endif
                
            endif
        endfor
        
        if (i == 1 && isRefAbove) || (i == 2 && ~isRefAbove)
            vLinesRef = vLines2;
        else
            vLinesSample = vLines2;
        endif
    endfor

    % ==========================================================================
    % PARTE 4: identificacao dos quadrados

    % funcao para desenhar um quadrado na imagem, com borda de 2px de largura
    % [t]op, [b]ottom, [r]ow, [c]olumn
    function I2 = drawSquare(I, tr, tc, br, bc)
        I2 = I;
        c = [50 255 50];    % cor da borda
        for i = 1:size(I2, 3)
            I2((tr-1):tr, tc:bc, i) = c(i);   % top border
            I2(tr:br, bc:(bc+1), i) = c(i);   % right border
            I2(br:(br+1), tc:bc, i) = c(i);   % bottom border
            I2(tr:br, (tc-1):tc, i) = c(i);   % left border
        endfor
    endfunction


    if isRefAbove
        hLinesRef = hLines(1:4, :);
        hLinesSample = hLines(5, :);
    else
        hLinesSample = hLines(1, :);
        hLinesRef = hLines(2:5, :);
    endif


    squaresImg = zeros([nRows nCols 3]);

    % Quadrados da referencia; cada linha contem 4 elementos: [tr tc br bc], 
    % que sao as coordenadas dos cantos superior esquerdo e inferior direito
    % de cada quadrado da referencia; sao 4 quadrados por pH, portanto 4 linhas.
    %
    % A 3a dimensao corresponde a cada valor de pH da referencia
    %
    %   squaresRef(:,:,  1) => pH  0
    %   squaresRef(:,:,  2) => pH  1
    %   ...
    %   squaresRef(:,:,  8) => pH  7    % pH 7 e repetido na cartela
    %   squaresRef(:,:,  9) => pH  7
    %   squaresRef(:,:, 10) => pH  8
    %   squaresRef(:,:, 11) => pH  9
    %   squaresRef(:,:, 12) => pH 10
    %   ...
    %   squaresRef(:,:, 16) => pH 14
    %
    squaresRef = [];

    % identificacao dos quadrados da referencia
    H = hLinesRef;      % linhas horizontais
    V = vLinesRef;      % linhas verticais

    % A identificacao foi dividida em duas partes, primeiro os 8 pares da esquerda
    % pra direita, depois os 8 pares da direita pra esquerda, de forma a ignorar
    % as linhas do centro da cartela

    for j = 1:8         % para cada par de linhas verticais do lado esquerdo
        ref = [];
        for i = 1:4     % para cada par de linhas horizontais
            ref = [ref; H(i, 1) V(j, 1) H(i, 2) V(j, 2)];
            squaresImg = drawSquare(squaresImg, H(i, 1), V(j, 1), H(i, 2), V(j, 2));
        endfor
        squaresRef = cat(3, squaresRef, ref);
    endfor

    % numero total de pares verticais, necessario pois ha amostras que contem
    % pares no meio e outras que nao contem
    vPairs = size(V, 1);

    for j = (vPairs-7):vPairs       % para cada par de linhas verticais do lado direito
        ref = [];
        for i = 1:4     % para cada par de linhas horizontais
            ref = [ref; H(i, 1) V(j, 1) H(i, 2) V(j, 2)];
            squaresImg = drawSquare(squaresImg, H(i, 1), V(j, 1), H(i, 2), V(j, 2));
        endfor
        squaresRef = cat(3, squaresRef, ref);    
    endfor


    % Quadrados da amostra; cada linha contem 4 elementos: [tr tc br bc], 
    % que sao as coordenadas dos cantos superior esquerdo e inferior direito
    % de cada quadrado da amostra.
    squaresSample = [];

    H = hLinesSample;   % linhas horizontais (so contem 1 par, entao so precisa de 1 for)
    V = vLinesSample;  % linhas verticais

    for j = 1:4         % para cada par de linhas verticais
        squaresSample = [squaresSample; H(1) V(j, 1) H(2) V(j, 2)];
        squaresImg = drawSquare(squaresImg, H(1), V(j, 1), H(2), V(j, 2));
    endfor
    
    openImg(1 - hHistImg);
    openImg(1 - vHistImg);
    openImg(255 - squaresImg);

endfunction    