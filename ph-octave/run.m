
## Testa o algoritmo em todas as imagens de amostra
pkg load statistics
pkg load image
warning("off");

inputImg=imread("process.jpg");
[sqSample, sqRef] = detectSquares(inputImg);
[str,L_ref,a_ref,b_ref] = extractColors(inputImg, sqSample, sqRef);
ph = detectPh(str,L_ref,a_ref,b_ref);
printf("%d\n", ph)