##
## openImg(I)
##
## Salva a matriz I como um arquivo de imagem e abre-a no Irfanview
## para melhor visualizacao, zoom, etc. Em seguida, apaga o arquivo
## criado.
##  
## Necessario ter o Irfanview instalado e adicionar a pasta de instalacao
## no PATH do sistema
##
function openImg(I)
    %imwrite(I, ".temp.bmp");
    %system("i_view32.exe .temp.bmp", false, "async");
    %pause(1);
    %delete(".temp.bmp");
endfunction