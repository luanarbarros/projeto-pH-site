function [ph] = detectPh(str,L_ref,a_ref,b_ref)
    %Para cada coluna de referencia
    for m_coluna = 1:16

        %Transforma os 3 arrays de 4 elementos contendo as medias de uma coluna de referencia
        %em uma matrix 4x3, onde cada linha contem as medias de um quadrado
        ref = [L_ref(m_coluna,:); a_ref(m_coluna,:); b_ref(m_coluna,:)]';

        %Para cada quadrado
        for n_quadrado = 1:4
            %Calcula a distância dos quadrados da referencia para os quadrados da fita
            distancia_cores_quadrados(n_quadrado,m_coluna) = pdist2(str(n_quadrado,:), ref(n_quadrado,:));
        end
    end

    somatorio_distancias = [];

    %Para cada coluna da referencia
    for m_coluna = 1:16
        %Realiza um somatorio das distancias
        somatorio_distancias(m_coluna) = sum(abs(distancia_cores_quadrados(:,m_coluna)));
    end

    %O pH eh definido sendo como a coluna com a menor soma das distancias.
   ph = find (somatorio_distancias == min (somatorio_distancias));

   %A referencia vai de 0 ate 7 e depois vai de 7 ate 14
   %ou seja, a referencia 1 ate 8 equivale a primeira parte,
   %que o pH varia de 0 ate 7, entao eh necessario subtrair um
   if (ph <= 8)
        ph = ph - 1;
    end

    %A referencia vai de 0 ate 7 e depois vai de 7 ate 14
    %ou seja, a referencia 9 ate 16 equivale a primeira parte,
    %que o pH varia de 7 ate 14, entao eh necessario subtrair dois
    if (ph > 8)
        ph = ph - 2;
    end

end