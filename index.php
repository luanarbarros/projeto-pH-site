
<?php

include("header.php");

session_start();

$usuario = null;

if(isset($_SESSION['usuario'])) {
	$usuario = $_SESSION['usuario'];
}

?>
		<!-- Banner -->
			<section id="banner">
				<header>
					<h2>pH Meter Online</h2>
					<p>Measure and keep the pH of several environments</p>
				</header>
			</section>

		<!-- Intro -->
			<section id="intro" class="container">
				<div class="row">
					<div class="col-4 col-12-medium">
						<section class="first">
							<i class="icon featured fa-leaf"></i>
							<header>
								<h2>Healthy Environments</h2>
							</header>
							<p>Keep the pH at the right level for each environment.</p>
						</section>
					</div>
					<div class="col-4 col-12-medium">
						<section class="middle">
							<i class="icon featured alt fa-line-chart"></i>
							<header>
								<h2>Statistic Data</h2>
							</header>
							<p>Keep informed of pH variation in each environment, generating graphs and statistical reports.</p>
						</section>
					</div>
					<div class="col-4 col-12-medium">
						<section class="last">
							<i class="icon featured alt2 fa-map-marker"></i>
							<header>
								<h2>Map Measurements</h2>
							</header>
							<p>Have the exact location of where you made your measurements.</p>
						</section>
					</div>
				</div>
				<footer id="loginIns">
					<ul class="actions">

						<?php
							if($usuario != null) {
						?>
								<li><a href="submit.php" class="button large">Get Started</a></li>
								<li><a href="logout.php" class="button alt large">Logout</a></li>
						<?php
							} else {
						?>
							<li><a href="#insertLogin" class="button alt large" onclick="javascript:toggleShow('insertLogin');">Login</a></li>
							<li><a href="#insertRegister" class="button alt large" onclick="javascript:toggleShow('insertRegister');">Register</a></li>
						<?php
							}
						?>

					</ul>

					<span id="insertLogin" style="display:none;">
						<div class="limiter">
							<div class="container-login100 p-b-1">
							<div class="wrap-login100 p-b-1">
								<form class="login100-form validate-form" action="login.php" method="POST">
								<span class="login100-form-title p-b-10">
									Account Login
								</span>
								
								<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-10" data-validate="Type user name">
									<input class="input100" type="email" name="email" placeholder="Email">
									<span class="focus-input100"></span>
								</div>
								<div class="wrap-input100 rs2-wrap-input100 validate-input m-b-10" data-validate="Type password">
									<input class="input100" type="password" name="pass" placeholder="Password">
									<span class="focus-input100"></span>
								</div>
								
								<div class="container-login100-form-btn">
									<button class="login100-form-btn">
									Sign in
									</button>
								</div>

								<div class="w-full text-center p-t-10 p-b-10">
									<span class="txt1">
									Forgot
									</span>

									<a href="#" class="txt2">
									User name / password?
									</a>
								</div>

								<div class="w-full text-center">
									<a href="#" class="txt3">
									Sign Up
									</a>
								</div>
								</form>
							</div>
							</div>
						</div>
					</span>
					<span id="insertRegister" style="display:none;">
						<div class="limiter">
							<div class="container-login100 p-b-1">
								<div class="wrap-login100 p-b-1">
								<form action="register.php" method="POST" class="login100-form validate-form">
									<span class="login100-form-title p-b-10">
									Account Register
									</span>
									
									<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-10" data-validate="Type user name">
									<input id="first-name-register" class="input100" type="text" name="username" placeholder="User name">
									<span class="focus-input100"></span>
									</div>
									<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-10" data-validate="Type user name">
									<input id="email" class="input100" type="email" name="email" placeholder="E-mail">
									<span class="focus-input100"></span>
									</div>
									<div class="wrap-input100 rs2-wrap-input100 validate-input m-b-10" data-validate="Type password">
									<input class="input100" type="password" name="pass" placeholder="Password">
									<span class="focus-input100"></span>
									</div>
									
									<div class="container-login100-form-btn">
									<button class="login100-form-btn">
										Sign in
									</button>
									</div>
								</form>
								</div>
							</div>
							</div>
					</span>
				</footer>
			</section>

	</section>

<!-- Main -->
	<section id="main">
		<div class="container">
			<div class="row">
				<div class="col-12">

				</div>
			</div>
		</div>
	</section>
	<script src="assets/js/login.js"></script>

<?php

include("footer.php"); 

if(isset($_SESSION["msg"])) {
	echo "<script> alert('".$_SESSION["msg"]."')</script>";
	unset($_SESSION['msg']);
}

?>